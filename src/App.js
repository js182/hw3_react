import React, {useState, useEffect} from 'react'
import axios from "axios"
import "./App.css";
import ProductList from "./components/ProductList/ProductList";
import Header from "./components/Header/Header";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import FavoritesList from "./components/Favorites/FavoritesList";
import BasketList from "./components/Cart/BasketList";


// Создаем функциональный компонент App
const App = () => {
  // Используем хук useState для создания состояний products и cartItems
  const [products, setProducts] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [cartCount, setCartCount] = useState(0);
  const [favoritesCount, setFavoritesCount] = useState(0);

  // Используем хук useEffect для асинхронной загрузки данных при монтировании компонента
  useEffect(() => {
    axios.get('products.json')
      .then((response) => {
        if (Array.isArray(response.data)) {
          setProducts(response.data);
          loadCartItems();
        } else {
          console.error('Data from products.json is not an array');
        }
      })
      .catch((error) => {
        console.error(`Error loading data from products.json: ${error}`);
      });
  }, []); // пустой массив зависимостей означает, что этот useEffect будет выполнен только при монтировании компонента

  // Функция для загрузки товаров в корзине из localStorage
  const loadCartItems = () => {
    const cart = JSON.parse(localStorage.getItem("cart")) || [];
    setCartItems(cart.length);
  };

  // Рендерим компонент App
  return (
    <Router>
      <div className="App">
        <Header cartCount={cartCount} favoritesCount={favoritesCount}/>
        <Routes>
          <Route path="/" element={<ProductList data={products}
                                                setFavoritesCount={setFavoritesCount}
                                                setCartCount={setCartCount}/>}/>
          <Route path="/cart" element={<BasketList setCartCount={setCartCount}/>}/>
          <Route path="/favorites" element={<FavoritesList setFavoritesCount={setFavoritesCount}/>}/>
        </Routes>
      </div>
    </Router>
  );
}

export default App;