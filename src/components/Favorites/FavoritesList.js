import React, {useState, useEffect} from 'react';
import ProductCard from "../ProductCard/ProductCard";
import s from './favorites.module.scss';

const FavoritesList = ({setFavoritesCount}) => {
  const [favoriteItems, setFavoriteItems] = useState([]);

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    setFavoriteItems(favorites);
  }, []);

  const removeFromFavorites = (sku) => {
    const newCart = favoriteItems.filter(product => product.sku !== sku);
    setFavoriteItems(newCart);
    setFavoritesCount(newCart.length)
    localStorage.setItem('favorites', JSON.stringify(newCart));
  }

  return (
    <div className={s.wrap}>
      {favoriteItems.map((product) => (
        <ProductCard key={product.sku} product={product} removeFromFavorites={removeFromFavorites}/>
      ))}
    </div>
  );
};
export default FavoritesList;