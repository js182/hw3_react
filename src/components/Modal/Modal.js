import React from "react";
import "./modal.scss";

class Modal extends React.Component {
  handleOutsideClick = (e) => {
    if (e.target === e.currentTarget) this.props.onClose();
  };

  render() {
    const {header, closeButton, text, actions} = this.props;

    return (
      <div className="ModalOverlay" onClick={this.handleOutsideClick}>
        <div className="Modal">
          <div className="ModalHeader">
            {header}
            {closeButton && <button onClick={this.props.onClose}>X</button>}
          </div>
          <div className="ModalBody">{text}</div>
          <div className="ModalFooter">{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;