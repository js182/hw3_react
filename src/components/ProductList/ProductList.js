import React, {useState} from 'react';
import PropTypes from 'prop-types'
import ProductCard from '../ProductCard/ProductCard';
import s from "./productList.module.scss"

const ProductList = ({data, setCartCount, setFavoritesCount}) => {
  const removeFromFavorites = (sku) => {
    const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
    const newCart = favorites.filter(product => product.sku !== sku);
    setFavoritesCount(newCart.length)
    localStorage.setItem('favorites', JSON.stringify(newCart));
  }

  return (
    <div className={s.wrap}>
      {data.map((product) => (
        <ProductCard
          setCartCount={setCartCount}
          key={product.sku}
          product={product}
          removeFromFavorites={removeFromFavorites}
          setFavoritesCount={setFavoritesCount}
        />
      ))}
    </div>
  );
};

ProductList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      sku: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      color: PropTypes.string.isRequired,
    })
  ).isRequired,

};

export default ProductList;



