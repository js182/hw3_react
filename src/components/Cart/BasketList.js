import React, {useEffect, useState} from 'react';
import ProductCard from '../ProductCard/ProductCard';
import s from './cart.module.scss';

const BasketList = ({setCartCount}) => {
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    const cart = JSON.parse(localStorage.getItem('cart')) || [];
    setCartItems(cart);
  }, []);


  const handleRemoveFromCart = (skuToRemove) => {
    const newCart = cartItems.filter(product => product.sku !== skuToRemove);
    setCartItems(newCart);
    setCartCount(newCart.length)
    localStorage.setItem('cart', JSON.stringify(newCart));
  };

  return (
    <div className={s.wrap}>
      {cartItems.map((product) => (
        <ProductCard key={product.sku} product={product} removeFromCart={handleRemoveFromCart}/>
      ))}
    </div>
  );
};

export default BasketList;